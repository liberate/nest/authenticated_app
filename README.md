# AuthenticatedApp

This is a mountable Rails Engine that provides basic authentication and session management.

## WARNING

* The password reset mechanism can be used to determine the recovery emails of users,
  particularly if email only option is set. There should be a rate limit or puzzle.
  (although, reset by email does not work with the 'email' column of a user, only
  the recovery_email column)

## TODO

* Better define requirements (haml, bootstrap, User methods)
* Add option to re-enable a user when the user successfully logs in.
* Add options for flash, etc.

## Requirements

* `haml` gem
* `User` class
* Bootstrap 5

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'authenticated_app'
```

## Configuration

`config/initializers/authenticated_app.rb`

```ruby
AuthenticatedApp.home_url = :home_url  # home for authenticated users
AuthenticatedApp.root_url = :root_url  # home for unauthenticated users
AuthenticatedApp.email_only = false    # if true, use email for login.
```

## Integration

```ruby
class ApplicationController < ActionController::Base
  include AuthenticatedAppConcern
end
```

In `routes.rb`:

```ruby
mount AuthenticatedApp::Engine => "/auth"
```
