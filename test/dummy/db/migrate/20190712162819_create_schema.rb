class CreateSchema < ActiveRecord::Migration[5.2]
  def change
    create_table "users", force: :cascade do |t|
      t.string "username"
      t.string "domain"
      t.string "email", limit: 191
      t.string "display_name"
      t.date "created_on"
      t.date "updated_on"
      t.date "last_login_on"
      t.boolean "is_active", default: true
      t.boolean "is_suspended", default: false
      t.boolean "is_pending", default: false
      t.boolean "is_confirmed", default: false
      t.boolean "keep_me", default: false
      t.integer "closed_by_id"
      t.integer "closed_reason"
      t.string "primary_secret"
      t.string "vpn_secret"
      t.string "chat_secret"
      t.string "mail_secret"
      t.string "storage_secret"
      t.string "recovery_secret"
      t.string "recovery_email"
      t.string "recovery_token"
      t.datetime "recovery_token_expires_at"
      t.index ["email"], name: "index_users_on_email", unique: true
      t.index ["username", "domain"], name: "index_users_on_username_and_domain", unique: true
    end
  end
end
