# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_12_162819) do

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "domain"
    t.string "email", limit: 191
    t.string "display_name"
    t.date "created_on"
    t.date "updated_on"
    t.date "last_login_on"
    t.boolean "is_active", default: true
    t.boolean "is_suspended", default: false
    t.boolean "is_pending", default: false
    t.boolean "is_confirmed", default: false
    t.boolean "keep_me", default: false
    t.integer "closed_by_id"
    t.integer "closed_reason"
    t.string "primary_secret"
    t.string "vpn_secret"
    t.string "chat_secret"
    t.string "mail_secret"
    t.string "storage_secret"
    t.string "recovery_secret"
    t.string "recovery_email"
    t.string "recovery_token"
    t.datetime "recovery_token_expires_at"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["username", "domain"], name: "index_users_on_username_and_domain", unique: true
  end

end
