require_relative "../../test_helper.rb"

class AuthenticatedApp::ResetWithEmailControllerTest < ActionDispatch::IntegrationTest
  include AuthenticatedApp::Engine.routes.url_helpers

  def setup
    @routes = AuthenticatedApp::Engine.routes
    @user = User.create!(username: 'manatee', recovery_email: 'manatee@florida.park', password: 'jfg928gh101')
    AuthenticatedApp.from_address = "test@example.org"
    AuthenticatedApp.email_only = false
  end

  def test_step_1
    get reset_with_email_path
    assert_response :success
    assert true
  end

  def test_step_2_success
    assert_difference 'ActionMailer::Base.deliveries.size' do
      put reset_with_email_check_email_path, params: {recovery_user: 'manatee', recovery_email: 'manatee@florida.park'}
      assert_equal "Check your email for the reset token", flash[:success]
      assert_response :redirect
    end

    email = ActionMailer::Base.deliveries.last
    assert_equal I18n.t(:reset_password), email.subject
    assert_equal 'manatee@florida.park', email.to[0]
    assert_match(/To reset the password, please visit this link/, email.body.to_s)
  end

  def test_step_2_error
    assert_no_difference 'ActionMailer::Base.deliveries.size' do
      put reset_with_email_check_email_path, params: {recovery_user: 'manatee', recovery_email: ''}
      assert_response :success
      assert instance(:errors)[:recovery_email]
      put reset_with_email_check_email_path, params: {recovery_user: 'manatee', recovery_email: 'xxxx'}
      assert_response :success
      assert instance(:errors)[:recovery_email]
      assert_nil instance(:errors)[:recovery_user]
      put reset_with_email_check_email_path, params: {recovery_user: 'manatee', recovery_email: 'xxxx@example.org'}
      assert_response :success
      assert instance(:errors)[:recovery_email]
      assert instance(:errors)[:recovery_user]
    end
  end

  def test_step_2_success_email_only
    AuthenticatedApp.email_only = true
    @user.update_columns(email: 'manatee@florida.park')
    assert_difference 'ActionMailer::Base.deliveries.size' do
      put reset_with_email_check_email_path, params: {recovery_email: 'manatee@florida.park'}
      assert_equal "Check your email for the reset token", flash[:success]
      assert_response :redirect
    end
  end

  def test_step_3_success
    @user.create_recovery_token('manatee@florida.park')
    get reset_with_email_enter_password_path, params: {recovery_user: 'manatee', token: @user.recovery_token}
    assert_response :success
    assert instance(:errors).empty?
  end

  def test_step_3_error
    get reset_with_email_enter_password_path
    assert_response :success
    assert instance(:errors)[:recovery_user]

    get reset_with_email_enter_password_path, params: {recovery_user: 'manatee', token: 'xxxx'}
    assert_response :success
    assert instance(:errors)[:token]
  end

  def test_step_4_success
    @user.create_recovery_token('manatee@florida.park')
    put reset_with_email_update_password_path, params: {
      recovery_user: 'manatee', token: @user.recovery_token, new_password: '91x4g61hp21', password_confirmation: '91x4g61hp21'
    }
    assert instance(:errors).empty?, instance(:errors)
    assert_response :redirect
    assert_nil @user.reload.recovery_token
  end

  def test_step_4_success_with_cleartext_recovery_email
    @user.create_recovery_token('manatee@florida.park')
    test_step_4_success
  end

  def test_step_4_error
    put reset_with_email_update_password_path
    assert instance(:errors)[:recovery_user]

    put reset_with_email_update_password_path, params: {recovery_user: 'manatee'}
    assert instance(:errors)[:token]

    @user.create_recovery_token('manatee@florida.park')
    put reset_with_email_update_password_path, params: {
      recovery_user: 'manatee', token: @user.recovery_token, new_password: '91x4g61hp21'
    }
    assert instance(:errors)[:password_confirmation]
  end

  def xtest_update_with_storage_key
    user = users(:blue)
    user.create_storage_key!(password: 'password')
    old_public_key = user.storage_key.public_key
    post :create, username: 'blue', recovery_email: "blue2@" + Conf.domain
    put :update, username: 'blue', token: user.reload.password_token,
      new_password: 'jcfu476489ghak', password_confirmation: 'jcfu476489ghak'
    assert_response :success
    assert_select '.recovery-code', @recovery_code
    assert_not_equal old_public_key, user.storage_key.public_key
  end

end
