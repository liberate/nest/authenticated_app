require_relative "../../test_helper.rb"

class ResetPasswordControllerTest < ActionController::TestCase

  PASSWORD = 'password'
  NEW_PASSWORD = 'ShardBratSinaiEraStuartYacht'

  def test_funky_codes
    code = RandomCode::create(Conf.recovery_code_length, Conf.recovery_code_split)
    user = users(:red)
    user.update_recovery!(password: PASSWORD, new_recovery: code)

    put :update, username: 'red', recovery_code: "wrong code"
    assert_response :success
    assert flash[:danger].present?, 'should be an error'

    put :update, username: 'red', recovery_code: code
    assert_response :success
    assert !flash[:danger].present?, 'should authenticate using recovery code'

    put :update, username: 'red', recovery_code: code.upcase
    assert_response :success
    assert !flash[:danger].present?, 'should support case insensitive codes'

    put :update, username: 'red', recovery_code: code.gsub('-', '')
    assert_response :success
    assert !flash[:danger].present?, 'should support codes without without hypens'
  end

  def test_change_password
    code = RandomCode::create(Conf.recovery_code_length, Conf.recovery_code_split)
    user = users(:red)
    old_digest = user.crypted_password
    user.update_recovery!(password: PASSWORD, new_recovery: code)

    put :update, username: 'red', recovery_code: "wrong code", stage: 'password',
      new_password: NEW_PASSWORD, password_confirmation: NEW_PASSWORD
    assert_equal old_digest, user.reload.crypted_password, 'password must not change'

    put :update, username: 'red', recovery_code: code.gsub('-','').upcase, stage: 'password',
      new_password: NEW_PASSWORD, password_confirmation: NEW_PASSWORD
    assert_not_equal old_digest, user.reload.crypted_password, 'password must change'
  end

end
