require_relative "../../test_helper.rb"

class ResetPasswordTokenControllerTest < ActionController::TestCase
  def test_new
    get :new
    assert_response :success
  end

  def test_show
    get :show
    assert_response :success
  end

  def test_create_with_username
    post :create
    assert flash[:danger].present?

    post :create, username: "red", recovery_email: "bad@"+Conf.domain
    assert flash[:danger].present?

    assert_difference 'ActionMailer::Base.deliveries.size' do
      post :create, username: "red", recovery_email: "red2@"+Conf.domain
    end
    assert_response :success
    assert assigns(:success_message)

    email = ActionMailer::Base.deliveries.last
    assert_equal I18n.t(:reset_password), email.subject
    assert_equal "red2@"+Conf.domain, email.to[0]
    assert_match /To reset the password, please visit this link/, email.body.to_s
  end

  def test_create_with_email
    assert_difference 'ActionMailer::Base.deliveries.size' do
      post :create, username: "red@"+Conf.domain, recovery_email: "red2@"+Conf.domain
    end
    assert assigns(:success_message)
  end

  def test_bad_update
    put :update
    assert flash[:danger].present?
  end

  def test_bad_update2
    put :update, username: "red", token: "bad"
    assert flash[:danger].present?
  end

  def test_update_with_hashed_recovery_email
    assert_match(/argon/, users(:red).recovery_email)
    post :create, username: "red", recovery_email: "red2@"+Conf.domain
    assert flash[:danger].blank?, flash[:danger]
    put :update, username: "red", token: users(:red).reload.password_token,
      new_password: "jcfu476489ghak", password_confirmation: "jcfu476489ghak"
    assert flash[:danger].blank?, flash[:danger]
    assert_response :success
  end

  def test_update_with_cleartext_recovery_email
    user = users(:blue)
    old_digest = user.crypted_password
    assert_no_match(/argon/, user.recovery_email)
    post :create, username: "blue", recovery_email: "blue2@"+Conf.domain
    assert flash[:danger].blank?, flash[:danger]
    put :update, username: "blue", token: user.reload.password_token,
      new_password: "jcfu476489ghak", password_confirmation: "jcfu476489ghak"
    assert flash[:danger].blank?, flash[:danger]
    assert_response :success
    user.reload
    assert_not_equal old_digest, user.crypted_password
    assert_equal "{ARGON2I}" + user.crypted_password, user.mailbox.password
  end

  def test_update_with_storage_key
    user = users(:blue)
    user.create_storage_key!(password: 'password')
    old_public_key = user.storage_key.public_key
    post :create, username: 'blue', recovery_email: "blue2@" + Conf.domain
    put :update, username: 'blue', token: user.reload.password_token,
      new_password: 'jcfu476489ghak', password_confirmation: 'jcfu476489ghak'
    assert_response :success
    assert_select '.recovery-code', @recovery_code
    assert_not_equal old_public_key, user.storage_key.public_key
  end

end
