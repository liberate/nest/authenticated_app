require_relative "../../test_helper.rb"

class AuthenticatedApp::PasswordsControllerTest < ActionController::TestCase
  def setup
    @routes = AuthenticatedApp::Engine.routes
    @user = create_user
    session[:user_id] = @user.id
  end

  def test_show
    get :show
    assert_response :success
  end

  def test_should_not_update_with_wrong_password
    put :update, params: {update_action: "update_password", current_password: "wrong password"}
    assert_response :success
    assert flash[:danger].present?, flash[:danger]
  end

  def test_should_not_update_without_confirmation
    put :update, params: {update_action: "update_password", current_password: @user.password, new_password: "93g92020h1g"}
    assert_response :success
    assert flash[:danger].present?, flash[:danger]
  end

  def test_should_update
    new_pass = "93g92020h1g"
    put :update, params: {
      update_action: "update_password",
      current_password: @user.password,
      new_password: new_pass,
      password_confirmation: new_pass
    }
    assert_response :redirect
    assert !flash[:danger].present?, 'should be no errors: ' + flash[:danger].inspect
    @user.reload
    assert @user.authenticated?(new_pass), "should authenticate with new password"
  end

  def test_update_recovery_code
    assert @user.recovery_secret.blank?

    put :update, params: {update_action: "update_recovery", current_password: "wrong password"}
    assert_response :success
    assert flash[:danger].present?, 'error should be set'

    put :update, params: {update_action: "update_recovery", current_password: @user.password}
    assert_response :success
    @user.reload
    assert @user.recovery_secret, "recovery code should be set"
    assert_match(/argon/, @user.recovery_secret, "recovery code should use argon digest")
  end

  def test_update_recovery_email
    @user.update_attribute(:recovery_email, "blah@example.org")

    # clear without password
    put :update, params: {
      update_action: "update_recovery_email", clear_value: "true"
    }
    assert_response :redirect
    assert @user.reload.recovery_email.blank?

    # wrong password
    put :update, params: {
      update_action: "update_recovery_email",
      current_password: "wrong password",
      user: {recovery_email: 'root@riseup.net'}
    }
    assert_response :success
    assert flash[:danger].present?, 'should be an error message'
    assert @user.reload.recovery_email.blank?

    # not an email
    put :update, params: {
      update_action: "update_recovery_email",
      current_password: @user.password,
      user: {recovery_email: 'root'}
    }
    assert flash[:danger].present?, 'should be an error'
    assert @user.reload.recovery_email.blank?
    assert_response :success

    # should work
    put :update, params: {
      update_action: "update_recovery_email",
      current_password: @user.password,
      user: {recovery_email: 'root@riseup.net'}
    }
    assert_response :redirect
    assert @user.reload.recovery_email.present?
  end

end
