module AuthenticatedApp
  class Mailer < ApplicationMailer
    def alias_confirmation(address:, recipient:, link:)
      @link = link
      @address = address
      @recipient = recipient
      mail(to: recipient, subject: I18n.t(:confirm_new_alias))
    end

    def reset_password(user:, recipient:, link:)
      @reset_link = link
      @user = user
      mail(
        from: AuthenticatedApp.from_address,
        to: recipient,
        subject: I18n.t(:reset_password)
      )
    end
  end
end
