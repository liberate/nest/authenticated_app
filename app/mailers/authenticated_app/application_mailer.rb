module AuthenticatedApp
  class ApplicationMailer < ActionMailer::Base
    #default from: (Conf.from_address || "no-reply@" + Conf.domain)
    #default reply_to: (Conf.reply_address || "no-reply@" + Conf.domain)
    layout 'mailer'
  end
end
